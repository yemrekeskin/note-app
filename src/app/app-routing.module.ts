import { NoteDetailComponent } from './pages/note-detail/note-detail.component';
import { MainLayoutComponent } from './pages/main-layout/main-layout.component';
import { NoteListComponent } from './pages/note-list/note-list.component';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component : MainLayoutComponent,
    children: [
      {
        path: '',
        component: NoteListComponent
      },
      {
        path: ':id',
        component: NoteDetailComponent
      },
      {
        path: 'new',
        component: NoteDetailComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
