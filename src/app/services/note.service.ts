import { Injectable } from '@angular/core';
import { Note } from '../models/note.model';
import { v4 as uuid } from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class NoteService {

  notes : Note[] = new Array<Note>();

  constructor() { }

  getAll() {
    return this.notes;
  }

  get(id: string) : Note {
    return this.notes.filter(d => d.id === id)[0];
  }

  getId(note: Note) {
    return this.notes.indexOf(note);
  }

  add(note: Note) {
    note.id = uuid();
    let newLength = this.notes.push(note);
    let index = newLength - 1;
    return index;
  }

  update(id: string, title: string, body: string) {
    let note = this.get(id);
    note.title = title;
    note.body = body;
  }

  delete(id: string) {
    this.notes = this.notes.filter(d=> d.id !== id);
  }


}
