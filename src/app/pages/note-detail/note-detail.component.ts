import { NoteService } from './../../services/note.service';
import { Note } from './../../models/note.model';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-note-detail',
  templateUrl: './note-detail.component.html',
  styleUrls: ['./note-detail.component.scss']
})
export class NoteDetailComponent implements OnInit {

  note: Note =  new Note();
  noteId: string = "";
  new: boolean = false;

  constructor(
    private noteService: NoteService,
    private router: Router,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit(): void {

    this.route.params.subscribe((params: Params) => {

      if(params.id && params.id != "new") {
        this.note = this.noteService.get(params.id);
        console.log('detail note -' + this.note);
        this.noteId = params.id;
        this.new = false;
      } else {
        this.new = true;
      }
    });
  }

  onSubmit(form: NgForm) {
    console.log('onSubmit:clicked');
    console.log(form);
    console.log(form.value);

    if(this.new) {
      this.noteService.add(form.value);
    } else {
      this.noteService.update(this.noteId, form.value.title, form.value.body);
    }
    this.router.navigateByUrl('/');
  }

  cancel() {
    console.log('cancel:clicked');
    this.router.navigateByUrl('/');
  }

}
